export default {
  port: '4000',
  options: {
    title: process.env.npm_package_name,
    version: process.env.npm_package_version,
    description: 'FMC Check Health Client',
    termsOfService: 'reason to use the service',
    contact: {
      name: 'LD3',
      url: '',
      email: 'ld3@email.com',
    },
    license: {
      name: 'LD3',
      url: 'https://LD3.com',
    },
    schemes: ['http', 'https'],
    consumes: ['application/json'],
    produces: ['application/json'],
    tags: [
      {
        name: 'API',
        description: 'Check FMC health',
      },
    ],
  },
};
