import { exec } from 'child_process';

export default async function isRunning(proc) {
  return new Promise(((resolve, reject) => {
    const cmd = 'tasklist';
    if (proc === '') {
      resolve(false);
    }
    exec(cmd, (err, stdout, stderr) => {
      resolve(stdout.toLowerCase().indexOf(proc.toLowerCase()) > -1);
    });
  }));
}
