import express from 'express';
import swagger from 'swagger-spec-express';
import logger from '../library/logger';
import isRunning from './handler';

const router = express.Router();
swagger.swaggerize(router);

router.get('/', (req, res) => {
  logger.info('Nhan duoc request');
  res.sendStatus(200);
});

router.get('/fmc', async (req, res) => {
  logger.info('FMC Client: Nhan duoc request tu may chu!');
  // Check xem FMC co chay hay khong
  const status = await isRunning('fmc.exe');
  if (status === false) {
    logger.info('Phan mem FMC khong chay tren may!');
    res.send('Phan mem FMC khong chay tren may');
    return;
  }
  res.send('Phan mem FMC chay tren may').status(202);
  logger.info('Phan mem FMC chay tren may');
});

export default router;
