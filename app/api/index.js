import express from 'express';
import swagger from 'swagger-spec-express';
import swaggerUi from 'swagger-ui-express';
import config from '../../config';
import router from './router';
import logger from '../library/logger';

const app = express();

export default async function API() {
  swagger.reset();
  swagger.initialise(app, config.options);
  app.use(express.json());
  app.use('/', router);
  swagger.compile();
  app.use('/swagger-ui', swaggerUi.serve, swaggerUi.setup(swagger.json()));

  app.listen(config.port, () => {
    logger.info(`API chay tren cong ${config.port}`);
  });
}
