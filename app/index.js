import API from './api';
import logger from './library/logger';

(async () => {
  try {
    await API();
    logger.info('Chuong trinh khoi chay thanh cong!');
  } catch (error) {
    logger.error('Xuat hien loi khi khoi chay!', error);
  }
})();
